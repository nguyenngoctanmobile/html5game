var express =require('express');
var path = require('path');
var mainController = require("./src/controllers/mainController");
var app = express();

app.set("view engine","ejs");
app.set("views",path.join(__dirname,"src/views"));
app.use(express.static('src/public'));

mainController(app);
/*
app.listen("3000",function(err){
    console.log("=================Server===============");
})
*/
var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
 
app.listen(server_port, server_ip_address, function () {
  console.log( "Listening on " + server_ip_address + ", port " + server_port );
});
