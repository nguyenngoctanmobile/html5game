var express =require('express');
var path = require('path');
var bodyParser = require('body-parser');

module.exports = function(app){
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended:false}));
    app.get("/",function(req,res){
        
        res.render("index");
    });
    app.get("/DiceGame",function(req,res){
        res.render("dicegame");
    });
    app.get("/Cannonball",function(req,res){
        res.render("cannonballgame");
    });
    app.get("/Slingshot",function(req,res){
        res.render("slingshotgame");
    });
    app.get("/Memory",function(req,res){
        res.render("memorygame");
    });
    app.get("/Quiz1",function(req,res){
        res.render("quiz1game");
    });
    app.get("/Maze",function(req,res){
        res.render("mazegame");
    });

    /*
    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
        var err = new Error('File Not Found');
        err.status = 404;
        res.render("404");       
        });
    */

 
 
}