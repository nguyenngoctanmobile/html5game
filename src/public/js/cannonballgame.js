$("body").ready(function(){
    const canvasWidth = 400;
    const canvasHeight = 300;
    var boxx = 20;
    var boxy = 30;
    var boxwidth = 350;
    var boxheight = 250;
    var ballrad = 10;
    var boxboundx = boxwidth+boxx-ballrad;
    var boxboundy = boxheight +boxy-ballrad;
    var inboundx = boxx+ballrad;
    var inboundy = boxy+ballrad;
    var ballx = 50;
    var bally = 60;
    var ballvx = 4;
    var ballvy = 8;
    var  ctx;
    var hue = [[255,0,0],[255,255,0],[0,255,0],[0,255,255],[0,0,255],[255,0,255]];
    var grad;
    var color;
    initValue();
    initEvent();
    function initEvent(){
        $("#formfunction").on('submit',function(e){
            e.preventDefault();
        });
        $("#changefunction").on('click',function(e){
            var f = $("#formfunction");
            ballvx = Number(f.find("[name='hv']").val());
            ballvy = Number(f.find("[name='vv']").val());            
        })
    }
    function initValue(){
        ctx = document.getElementById("canvas").getContext('2d');       
        ctx.clearRect(0,0,canvasWidth,canvasHeight) ;
        ctx.lineWidth= 3;
        

        var h;
        grad = ctx.createLinearGradient(boxx, boxy, boxx+boxwidth, boxy+boxheight);;
        for(h = 0;h<hue.length;h++){
            color = 'rgb('+hue[h][0]+","+hue[h][1]+","+hue[h][2]+")";
            grad.addColorStop(h*1/6, color);
            
        }
        
        ctx.fillStyle = grad;//'rgb(200,0,50)';
        ctx.strokeStyle=grad;
        
        ctx.strokeRect(boxx,boxy,boxwidth,boxheight);
        moveBall();        
        ctx.lineWidth = ballrad;
        
        setInterval(moveBall,100);
    }
    function moveBall(){
        ctx.clearRect(boxx,boxy,boxwidth,boxheight);        
        moveAndCheck();       
        ctx.beginPath();
        
        ctx.arc(ballx,bally,ballrad,0,Math.PI*2,true);       
        ctx.closePath(); 
        ctx.fill();
        
        
    }
    function moveAndCheck(){
        var nballx = ballx+ballvx;
        var nbally = bally +ballvy;
        if(nballx >boxboundx ){
            ballvx = -ballvx;
            nballx = boxboundx;
        }
        if(nbally>boxboundy){
            ballvy = -ballvy;
            nbally =boxboundy;
        }
        if(nballx <inboundx ){
            ballvx = -ballvx;
            nballx = inboundx;
        }
        if(nbally<inboundy){
            ballvy = -ballvy;
            nbally =inboundy;
        }
        ballx = nballx;
        bally = nbally;
        
    }
});