$("body").ready(function(){
    let ctx;
    let cwidth=400;
    let cheight = 300;
    let dicex = 50;
    let dicey = 50;
    let dicewidth = 100;
    let diceheight = 100;
    let dotrad = 6;
    let dx;
    let dy;
    let firstturn = true;
    let point;
    
    init();
    initEvent();
    
    function init(){
        ctx = document.getElementById("canvas").getContext('2d');
        ctx.clearRect(0,0,cwidth,cheight);           
        var ch = 1+Math.floor(Math.random()*6);
        drawFace(ch);

    }
    function initEvent(){
        $("#throwDiceBtn").on("click",throwDice);
        $("#formControl").on("submit",function(event){
            event.preventDefault();
        });
      
    }
    function drawFace(n){
        ctx.lineWidth = 5;        
        ctx.clearRect(dx, dy, dicewidth, diceheight);
        ctx.strokeRect(dx, dy, dicewidth, diceheight);
        ctx.fillStyle='#009966';
        
            switch(n){
                case 1:
                    draw1();
                    break;
                case 2:
                    draw2();
                    break;
                case 3:
                    draw2();
                    draw1();
                    break;
                case 4:
                    draw4();                    
                    break;
                case 5:
                    draw4();
                    draw1();
                    break;
                case 6:
                    draw4();
                    draw2mid();
                    break;
            }
    }
    function draw1(){
        var dotx;
        var doty;
        ctx.beginPath();
        dotx = dx + .5*dicewidth;
        doty = dy + .5*diceheight;
        ctx.arc(dotx,doty,dotrad,Math.PI*2,true);
        ctx.closePath();
        ctx.fill();
        
    }
    function draw2(){
        var dotx;
        var doty;
        ctx.beginPath();
        dotx = dx+3*dotrad;
        doty = dy+3*dotrad;
        ctx.arc(dotx,doty,dotrad,0,Math.PI*2,true);
        dotx = dx+dicewidth-3*dotrad;
        doty = dy+diceheight-3*dotrad;
        ctx.arc(dotx,doty,dotrad,0,Math.PI*2,true);
        ctx.closePath();
        ctx.fill();
    }
    function draw4(){
        var dotx;
        var doty;
        ctx.beginPath();
        dotx = dx +3*dotrad;
        doty = dy +3*dotrad;
        ctx.arc(dotx,doty,dotrad,0,Math.PI*2,true);
        dotx = dx+dicewidth- 3*dotrad;
        doty = dy+diceheight-3*dotrad;
        ctx.arc(dotx,doty,dotrad,0, Math.PI*2,true);
        ctx.closePath();
        ctx.fill();
        ctx.beginPath();
        dotx = dx + 3*dotrad;
        doty = dy + diceheight -3*dotrad;
        ctx.arc(dotx,doty,dotrad,0, Math.PI*2,true);
        dotx = dx +dicewidth- 3*dotrad;
        doty = dy + 3*dotrad;
        ctx.arc(dotx,doty,dotrad,0, Math.PI*2,true);
        ctx.closePath();
        ctx.fill();
    }
    function draw2mid(){
        var dotx;
        var doty;
        ctx.beginPath();
        dotx = dx+ 3*dotrad;
        doty = dy+.5*diceheight;
        ctx.arc(dotx,doty,dotrad,0, Math.PI*2,true);
        dotx = dx+dicewidth-3*dotrad;
        doty = dy+.5*diceheight;
        ctx.arc(dotx,doty,dotrad,0,Math.PI*2,true);
        ctx.closePath();
        ctx.fill();
    }

    function throwDice(event){
        var sum;
        var ch = 1+Math.floor(Math.random()*6);
        sum = ch;
        dx = dicex;
        dy = dicey;
        drawFace(ch);
        dx = dicex + 150;
        ch = 1+Math.floor(Math.random()*6);
        sum +=ch;
        drawFace(ch);
        var f = $("#formControl");
        var outCome  = f.find ("[name='outcome']");
        var stage =  f.find("[name='stage']");
        var pv = f.find("[name='pv']");
        var bank = f.find("[name='bank']");
        var bankVal = Number(bank.val());
        if(bankVal<10){
            alert("You ran out of money! Add some more and try again");
            return;
        }
        bankVal = bankVal -10;
        bank.val(String(bankVal));
        if(firstturn){                 
            switch(sum){
                case 7:
                case 11:                   
                    outCome.val("You win!");                   
                    break;
                case 2:
                case 3:
                case 12:
                    outCome.val("You lose!");                   
                break;
                default:
                    point = sum;
                    pv.val(point);                    
                    stage.val('Need follow-up throw.');
                    outCome.val("");
                    firstturn = false;
                    
            }
        }else{
            switch(sum){
                case point:
                    outCome.val("You win!");
                    stage.val("Back to first");
                    pv.val("");
                    firstturn = true;
                case 7:
                    outCome.val("You lose!");
                    stage.val("Back to first");
                    pv.val("");
                    firstturn = true;
            }
        }
    }
    
});
