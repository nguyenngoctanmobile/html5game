$('body').ready(function(){
    var cwidth = $("#canvas").css("width");
    var cheight = $("#canvas").css("height");
    var ctx;
    var everything = [];
    var tid;
    var horvelocity;
    var verticalvel1;
    var verticalvel2;
    var gravity = 2;
    var iballx = 20;
    var ibally = 300;
    var cball;
    var target;
    var ground;
    var f = $("#formfunction");
    
    function Ball(sx,sy,rad,styleString){
        this.sx = sx;
        this.sy = sy;
        this.rad = rad;
        this.draw = drawBall;
        this.moveit = moveBall;
        this.fillstyle = styleString;        
    }
    function Myrectangle(sx,sy,swidth,sheight,styleString){
        this.sx = sx;
        this.sy = sy;
        this.sheight = sheight;
        this.swidth = swidth;
        this.fillstyle = styleString;
        this.draw = drawRects;
        this.moveit = moveBall;
    }

    function drawBall(){
        ctx.fillstyle = this.stylestring;
        ctx.beginPath();
               
        ctx.arc(this.sx,this.sy,this.rad,0, Math.PI*2,true);
        ctx.fill();
        ctx.closePath();            
    }
    function drawRects(){
        ctx.fillstyle = this.fillStyle;        
        ctx.fillRect(this.sx,this.sy,this.swidth,this.sheight);
    }
    function moveBall(dx,dy){
        this.sx +=dx;
        this.sy +=dy;

    }
    function fire(){
        
        cball.sx = iballx;
        cball.sy = ibally;
        horvelocity = Number(f.find("[name='hv']").val());
        verticalvel1 =Number(f.find("[name='vv']").val());
        console.log(horvelocity+":"+verticalvel1)
        drawAll();
        tid = setInterval(change,100);
        return false;
    }
    function drawAll(){
        ctx.clearRect(0,0,cwidth,cheight);
        for(i =0;i<everything.length;i++){
            everything[i].draw();
        }
    }
    function change(){       
        var dx = horvelocity;
        verticalvel2 = verticalvel1+gravity;
        var dy = (verticalvel1+verticalvel2)*0.5;
        verticalvel1 = verticalvel2;
        cball.moveit(dx,dy);
        var bx =cball.sx;
        var by = cball.sy;
        if((bx>=target.sx)&&(bx<=(target.sx+target.swidth))&& (by>=target.sy)&&(by<=(target.sy+target.sheight))){
            clearInterval(tid);
        }
        if(by>=ground.sy){
            clearInterval(tid);
        }        
        drawAll();
    }
    function init(){
        ctx = document.getElementById("canvas").getContext("2d");
        
        ctx.fillStyle = "rgb(250,0,0)";
        cball = new Ball(iballx,ibally,10,"rgb(250,0,0)");
        target = new Myrectangle(300,100,80,200,"rgb(0,5,90)");
        ground = new Myrectangle(0,300,600,"rgb(10,250,0)");
        everything.push(target);
        everything.push(ground);        
        everything.push(cball);
        drawAll();
                
    }
    
    function drawsling(){
        ctx.strokeStyle = strokeStyle;
        ctx.lineWidth = 4;
        ctx.beginPath();
        ctx.moveTo(this.bx,this.by);
        ctx.lineTo(this.s1x,this.s1y);
        ctx.moveTo(this.bx,this.by);
        ctx.lineTo(this.s2x,this.s2y);
        ctx.moveTo(this.s1x,this.s1y);
        ctx.lineTo(this.s2x,this.s2y);
        ctx.lineto(this.s3x,this.s3y);
        ctx.stroke();
    }
    function initEvent(){
        //ctx.on("mousedown",findBall);
       // ctx.on("mousemove",moveit);
        //ctx.on("mosueup",finish);
        $("#firebtn").on("click",fire);
        
    }
    
    init();
    initEvent();
})